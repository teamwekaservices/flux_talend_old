#!/bin/sh
cd `dirname $0`
ROOT_PATH=`pwd`
java -Dtalend.component.manager.m2.repository=$ROOT_PATH/../lib -Xms256M -Xmx1024M -cp .:$ROOT_PATH:$ROOT_PATH/../lib/routines.jar:$ROOT_PATH/../lib/dom4j-1.6.1.jar:$ROOT_PATH/../lib/jakarta-oro-2.0.8.jar:$ROOT_PATH/../lib/jsch-0.1.53.jar:$ROOT_PATH/../lib/log4j-1.2.17.jar:$ROOT_PATH/../lib/ojdbc7.jar:$ROOT_PATH/../lib/talendcsv.jar:$ROOT_PATH/flux16_consentementscomundi_email_dolist_0_6.jar: rcu.flux16_consentementscomundi_email_dolist_0_6.FLUX16_ConsentementsComundi_Email_Dolist  --context=PROD "$@"