%~d0
cd %~dp0
java -Dtalend.component.manager.m2.repository=%cd%/../lib -Xms256M -Xmx1024M -cp .;../lib/routines.jar;../lib/dom4j-1.6.1.jar;../lib/jakarta-oro-2.0.8.jar;../lib/jsch-0.1.53.jar;../lib/log4j-1.2.17.jar;../lib/postgresql-42.2.5.jar;../lib/talendcsv.jar;flux64_consentementsti_1_0.jar; rcu.flux64_consentementsti_1_0.FLUX64_ConsentementsTI  --context=Prod %*