#!/bin/sh
cd `dirname $0`
ROOT_PATH=`pwd`
java -Dtalend.component.manager.m2.repository=$ROOT_PATH/../lib -Xms256M -Xmx1024M -cp .:$ROOT_PATH:$ROOT_PATH/../lib/routines.jar:$ROOT_PATH/../lib/activation.jar:$ROOT_PATH/../lib/dom4j-1.6.1.jar:$ROOT_PATH/../lib/log4j-1.2.17.jar:$ROOT_PATH/../lib/mail-1.4.jar:$ROOT_PATH/../lib/ojdbc7.jar:$ROOT_PATH/../lib/talendcsv.jar:$ROOT_PATH/flux_86_tissot_sap_typologie_siret_0_2.jar: rcu.flux_86_tissot_sap_typologie_siret_0_2.FLUX_86_Tissot_SAP_typologie_SIRET  --context=PROD "$@"